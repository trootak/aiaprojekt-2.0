﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Esimene_2._0.Models;



namespace Esimene_2._0.Models

{
    [Authorize(Roles = "Administrator")]
    partial class AspNetUser
    {
        public static AspNetUser ByEmail(string email)
        {
            AednikudEntities db = new AednikudEntities();
            return db.AspNetUsers.Where(x => x.Email == email).SingleOrDefault();
        }
    }
}
namespace Esimene_2._0.Controllers
{

    [Authorize(Roles = "Administrator")]
    public class UsersController : Controller
    {
        private AednikudEntities db = new AednikudEntities();

        // GET: Users
        public ActionResult Index()
        {
           
          
             return View(db.AspNetUsers.ToList());
           
        }
       // [Authorize(Roles = "Administrator")]
        public ActionResult AddRole(string id, string roleId, string ret = "Details")
        {
            AspNetUser u = db.AspNetUsers.Find(id);
            AspNetRole r = db.AspNetRoles.Find(roleId);
            if (u != null && r != null)
            {
                try
                {
                    u.AspNetRoles.Add(r);
                    db.SaveChanges();
                }
                catch (Exception)
                {

                }
            }
            //            return View("Details", u);
            return RedirectToAction(ret, new { id = id });
        }
       // [Authorize(Roles = "Administrator")]
        public ActionResult RemoveRole(string id, string roleId, string ret = "Details")
        {
            AspNetUser u = db.AspNetUsers.Find(id);
            AspNetRole r = db.AspNetRoles.Find(roleId);
            if (u != null && r != null)
            {
                try
                {
                    u.AspNetRoles.Remove(r);
                    db.SaveChanges();
                }
                catch (Exception)
                {

                }
            }
            //            return View("Details", u);
            return RedirectToAction(ret, new { id = id });
        }
        // GET: Users/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            ViewBag.Roles = db.AspNetRoles.ToList()
                .Except(aspNetUser.AspNetRoles.ToList())
                .ToList();
            return View(aspNetUser);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,DisplayName,BirthDate,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] AspNetUser aspNetUser)
        {
            if (ModelState.IsValid)
            {
                db.AspNetUsers.Add(aspNetUser);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(aspNetUser);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            ViewBag.Roles = db.AspNetRoles.ToList()
                .Except(aspNetUser.AspNetRoles.ToList())
                .ToList();
            return View(aspNetUser);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,DisplayName,BirthDate,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] AspNetUser aspNetUser)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aspNetUser).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(aspNetUser);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUser);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            db.AspNetUsers.Remove(aspNetUser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
