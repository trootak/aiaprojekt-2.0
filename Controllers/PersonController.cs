﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Esimene_2._0.Models;
namespace Esimene_2._0.Models
{
    partial class Person
    {
        public string FullName => $"{FirstName} {LastName}";
        public string OwnerID => FullName;
    }
}

namespace Esimene_2._0.Controllers
{
    
    public class PeopleController : MyController
    {
        //private AednikudEntities db = new AednikudEntities();

        // GET: People
        public ActionResult Index()
        {
            //CheckPerson();
            //if (User.IsInRole("Administrator"))
            //{
                var people = db.People.Include(p => p.DataFile);
                return View(people.ToList());
            //}

            //return RedirectToAction("Index", "Home");
        }



        // GET: People/Details/5
        //public ActionResult Details(int? id)
        //{
        //    CheckPerson();


        //    id = id ?? CurrentPerson.Id;

        //    if (!User.IsInRole("Administrator") && id != CurrentPerson.Id)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        //    Person person = db.People.Find(id);
        //    if(person==null)
        //    {
        //        return HttpNotFound();
        //    }

        //    return View(person);

        //}
        public ActionResult Details(int? id)
        {
            CheckPerson();
            id = id ?? CurrentPerson.Id;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // GET: People/Create
        public ActionResult Lisa(string email)
        {
            if (email == "") return View("Create");
            ApplicationDbContext dba = new ApplicationDbContext();
            ApplicationUser u = dba.Users.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
            db.People.Add(new Person
            {
                Email = email,
                FirstName = u.FirstName,
                LastName = u.LastName,
                BirthDate = u.BirthDate
            });
            db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Create()
        {
            return View(new Person { Email = "" });
        }

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Email,FirstName,LastName,BirthDate")] Person person
           , HttpPostedFileBase file
            )
        {
            if (ModelState.IsValid)
            {
                db.People.Add(person);
                db.SaveChanges();
                ChangeDataFile(file, x => { person.PictureId = x; db.SaveChanges(); }, null);
                return RedirectToAction("Index");
            }

            
            
            return View(person);
        }

        
        // GET: People/Edit/5
        public ActionResult Edit(int? id)
        {
            CheckPerson();

            id = id ?? CurrentPerson.Id;

            if (!User.IsInRole("Administrator") && id != CurrentPerson.Id)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }

            return View(person);
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Email,FirstName,LastName,BirthDate,PictureId")] Person person, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.SaveChanges();
                ChangeDataFile(file, x => { person.PictureId = x; db.SaveChanges(); }, person.PictureId);
                return RedirectToAction("Index");
            }

            return View(person);
        }

        // GET: People/Delete/5
        public ActionResult Delete(int? id)
        {
            CheckPerson();

            id = id ?? CurrentPerson.Id;

            if (!User.IsInRole("Administrator") && id != CurrentPerson.Id)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }

            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            
            Person person = db.People.Find(id);
            db.People.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
