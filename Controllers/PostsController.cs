﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Esimene_2._0.Models;

namespace Esimene_2._0.Controllers
{
    //[Authorize(Roles = "User, Admin")]



    public class PostsController : MyController
    {        //Lisasin Otsi meetodi
        public ActionResult Otsi(string searching)
        {

            ViewBag.search = searching;
            return View(db.Posts.Where(x => x.Title.Contains(searching) || x.Content.Contains(searching) || searching == null).ToList());
         
        }

        //private AednikudEntities db = new AednikudEntities();

        // GET: Posts
        public ActionResult Index()
        {
            CheckPerson();
            ViewBag.CurrentPerson = CurrentPerson;
            if (User.IsInRole("Administrator"))
            {
                var posts = db.Posts.Include(p => p.DataFile).Include(p => p.Person).Include(p => p.Subject);
                return View(posts.ToList());
            }
            else
            {
                var posts = db.Posts.Include(p => p.DataFile).Include(p => p.Person).Include(p => p.Subject)
                .Where(x => x.OwnerId == CurrentPerson.Id)
              ;

                return View(posts.ToList());
            }
        }

        // GET: Posts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // GET: Posts/Create
        public ActionResult Create()
            
        {
            CheckPerson();

            ViewBag.OwnerId = new SelectList(db.People, "Id", "Email", CurrentPerson.Id);
            //ViewBag.OwnerId = CurrentPerson.Id;
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Name");
            return View( new Post { Created = DateTime.Now, OwnerId = CurrentPerson.Id});
        }

        // POST: Posts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,OwnerId,Created,SubjectId,PictureId,Title,Content,State")] Post post
            , HttpPostedFileBase file)
        {
            //if(NewSubject != "")
            //{
            //    Subject subject = new Subject { Name = NewSubject };
            //    db.Subjects.Add(subject);
            //    db.SaveChanges();
            //    post.SubjectId = subject.Id;
            //}
            if (ModelState.IsValid)
            {
                db.Posts.Add(post);
                db.SaveChanges();
                ChangeDataFile(file, x => { post.PictureId = x; db.SaveChanges(); }, null);
                return RedirectToAction("Index");
            }

            CheckPerson();
            ViewBag.OwnerId = new SelectList(db.People, "Id", "Email", post.OwnerId);
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Name", post.SubjectId);
            return View(post);
        }

        // GET: Posts/Edit/5
        public ActionResult Edit(int? id)
        {
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //Post post = db.Posts.Find(id);
            //if (post == null)
            //{
            //    return HttpNotFound();
            //}

            CheckPerson();

            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            if (User.IsInRole("Administrator") || post.OwnerId == CurrentPerson.Id)

            {

                ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Name", post.SubjectId);
                return View(post);
            }
            else return RedirectToAction("Index");
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,OwnerId,Created,SubjectId,PictureId,Title,Content,Access,State")] Post post
            , HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                ChangeDataFile(file, x => { post.PictureId = x; db.SaveChanges(); }, post.PictureId);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
           

            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Name", post.SubjectId);
            return View(post);
        }

        // GET: Posts/Delete/5
        public ActionResult Delete(int? id)
        {
            CheckPerson();

            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }

            if (!User.IsInRole("Administrator") || post.OwnerId == CurrentPerson.Id)
            {

                ViewBag.Delete = new SelectList(db.Subjects, "Id", "Name", post.SubjectId);
                return View(post);
            }
            else return RedirectToAction("Index");
        }

        // POST: Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
