﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Esimene_2._0.Models;

namespace Esimene_2._0.Controllers
{ 
    public class SubjectsController : Controller
    {
        private AednikudEntities db = new AednikudEntities();

        // GET: Subjects1
        public ActionResult Index()
        {
            var subjects = db.Subjects.Include(s => s.DataFile);
            return View(subjects.ToList());
        }

        // GET: Subjects1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subject subject = db.Subjects.Find(id);
            if (subject == null)
            {
                return HttpNotFound();
            }
            return View(subject);
        }

        // GET: Subjects1/Create
        public ActionResult Create()
        {
           
            return View();
        }

        // POST: Subjects1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description,PictureId")] Subject subject)
        {
            if (db.Subjects.Where(x => x.Name == subject.Name).Count() > 0)
                ModelState.AddModelError("Name", "sellise nimega subject meil juba on");


            if (ModelState.IsValid)
            {
                db.Subjects.Add(subject);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

           
            return View(subject);
        }

        // GET: Subjects1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subject subject = db.Subjects.Find(id);
            if (subject == null)
            {
                return HttpNotFound();
            }
            
            return View(subject);
        }

        // POST: Subjects1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,PictureId")] Subject subject)
        {
            if (ModelState.IsValid)
            {
                db.Entry(subject).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            return View(subject);
        }

        // GET: Subjects1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subject subject = db.Subjects.Find(id);
            if (subject == null)
            {
                return HttpNotFound();
            }
            return View(subject);
        }

        // POST: Subjects1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Subject subject = db.Subjects.Find(id);
            db.Subjects.Remove(subject);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }

//public partial class Subjects
//{
//    public static Dictionary<int, string> Kategooria = new Dictionary<int, string>
//    {
//        {0,"kõik" },
//        {1,"püsik" },
//        {2,"üheaastased" },
//        {3,"puud" },
//        {4,"sibullilled" },
//        {5,"toataimed" },
//        {6,"varia" },
//    };
//    public string SubjectName =>
//        Kategooria.ContainsKey(SubjectName) ? Kategooria[SubjectName] : "";
//}
}
